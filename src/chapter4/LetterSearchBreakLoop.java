package chapter4;

import com.sun.jmx.snmp.internal.SnmpSubSystem;

import java.util.Scanner;

/*
LOOP Break
Search a string to determine if it contains the letter 'A'
 */
public class LetterSearchBreakLoop {
    public static void main(String args[]){
        //Get Text
        System.out.println("Enter the text: ");
        Scanner scanner = new Scanner(System.in);
        String text = scanner.next();
        scanner.close();

        boolean letterFound = false;
        //condition
        for(int i=0; i<text.length(); i++){
            char currentLetter = text.charAt(i);
            if(currentLetter == 'A' || currentLetter == 'a'){
                letterFound = true;
                break;
                //version control check
                //re-check
            }
            }
        if(letterFound){
            System.out.println("This text contains the letter 'A'");
        }
        //work your best
        else{
            System.out.println("This text doesn't contain the letter 'A'");
        }
    }
}
