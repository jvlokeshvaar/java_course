package chapter3;

import java.util.Scanner;

/*
Logical Operators
To qualify for a loan, a person must take at least $30000
and have been working at their current job for at least 2 years
 */
public class LogicalOperatorLoanQualifier {
    public static void main(String args[]){
        //Initialize what we know
        int requiredSalary = 30000;
        int requiredYears = 2;

        //Get what we don't
        System.out.println("Enter your salary:");
        Scanner scanner = new Scanner(System.in);
        double salary = scanner.nextDouble();

        System.out.println("Enter the number of years with current employer:");
        double years = scanner.nextDouble();
        scanner.close();

        //Make decision
        if(salary >= requiredSalary && years >= requiredYears){
                System.out.println("Congrats! You are qualified for loan");
            }
        else{
            System.out.println("Sorry you must earn atleast" + requiredSalary + "to qualify for the loan");
        }
    }
}
