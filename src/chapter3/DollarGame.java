package chapter3;

import java.util.Scanner;

/*
Change for a dollar game
 */
public class DollarGame {
    public static void main(String args[]){
        //Known value
        double fiveCents = 0.05;
        double tenCents = 0.10;
        double twentyCents = 0.20;
        double twentyFiveCents = 0.25;
        double oneRinggit = 1.00;

        // User entry fields
        System.out.println("Enter the quantities for the following coins: fiveCents = ");
        Scanner scanner = new Scanner(System.in);
        double numOfFiveCents = scanner.nextDouble();

        System.out.println("Enter the quantities for the following coins: tenCents = ");
        double numOfTenCents = scanner.nextDouble();

        System.out.println("Enter the quantities for the following coins: twentyCents = ");
        double numOfTwentyCents = scanner.nextDouble();

        System.out.println("Enter the quantities for the following coins: twentyFiveCents = ");
        double numOfTwentyFiveCents = scanner.nextDouble();
        scanner.close();

        //Condition
        double total = fiveCents * numOfFiveCents + tenCents * numOfTenCents + twentyCents * numOfTwentyCents + twentyFiveCents * numOfTwentyFiveCents;

        if (total < oneRinggit){
            double amountShort = oneRinggit - total;
            System.out.println("Sorry you are short of " + amountShort + " cents.");
        }
        else if (total > oneRinggit) {
            double amountOver = total - oneRinggit;
            if (amountOver >= oneRinggit) {
                System.out.println("Sorry you are more of " + amountOver + " ringgit/s.");
            } else {
                System.out.println("Sorry you are more of " + amountOver + " cents.");
            }
        }
        else {
            System.out.println("You won!");
        }
    }
}
