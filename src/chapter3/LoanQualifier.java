package chapter3;

import java.util.Scanner;

/*
Nested IFs
To qualify for a loan, a person must take at least $30000
and have been working at their current job for at least 2 years
 */
public class LoanQualifier {
    public static void main(String args[]){
        //Initialize what we know
        int requiredSalary = 30000;
        int requiredYears = 2;

        //Get what we don't
        System.out.println("Enter your salary:");
        Scanner scanner = new Scanner(System.in);
        double salary = scanner.nextDouble();

        System.out.println("Enter the number of years with current employer:");
        double years = scanner.nextDouble();
        scanner.close();

        //Make decision
        if(salary >= requiredSalary){
            if(years >= requiredYears){
                System.out.println("Congrats! You are qualified for loan");
            }
            else{
                System.out.println("Sorry you must have worked at current job for" + requiredYears + "years.");
            }
        }
        else{
            System.out.println("Sorry you must earn atleast" + requiredSalary + "to qualify for the loan");
        }
    }
}
