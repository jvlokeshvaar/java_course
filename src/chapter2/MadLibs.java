package chapter2;

import java.util.Scanner;

public class MadLibs {
    public static void main(String arg[]){
        //Input 1: adjective
        System.out.println("Enter the adjective");
        Scanner scanner = new Scanner(System.in); //To read from the input
        String adjective = scanner.next();

        //Input 2: season of the year
        System.out.println("Enter the season of the year");
        String season = scanner.next();

        //Input 3: whole number
        System.out.println("Enter the whole number");
        String wholenumber = scanner.next();

        //Output
        System.out.println("On a" + adjective + season + "day, I drink a minimum of" + wholenumber + "cups of coffee");
    }
}
